#include <QtTest>
#include "key.h"

// add necessary includes here

class Test_Key : public QObject
{
    Q_OBJECT

private slots:
    void IncrementKey();
    void DecrementKey();
    void CopyMove();

};

void Test_Key::CopyMove()
{
    Key key;
    key.setKey("AA01-ZZ99");

    Key key2 = key;
    QCOMPARE(key.getKey(), "AA01-ZZ99");
    QCOMPARE(key2.getKey(), "AA01-ZZ99");

    Key key3(key);
    QCOMPARE(key.getKey(), "AA01-ZZ99");
    QCOMPARE(key3.getKey(), "AA01-ZZ99");

    Key key4 = std::move(key);
    QCOMPARE(key.getKey(), "AA00");
    QCOMPARE(key4.getKey(), "AA01-ZZ99");
}

void Test_Key::IncrementKey()
{
    Key key;
    QCOMPARE(key.getKey(), "AA00");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "AA01");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "AA02");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "AA04");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "AA06");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "AA08");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "AA09");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "AA10");

    key.setKey("AA99");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "AB00");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "AB01");

    key.setKey("ZZ99");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "AA01-AA00");

    key.setKey("AA01-ZZ99");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "AA02-AA00");

    key.setKey("ZZ99-ZZ99");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "AA01-AA00-AA00");

    key.setKey("ZZ99-ZZ99-ZZ99");
    key.IncrementKey();
    key.IncrementKey();
    key.IncrementKey();
    QCOMPARE(key.getKey(), "ZZ99-ZZ99-ZZ99");

    key.setKey("XX00-KL99");
    QCOMPARE(key.getKey(), "XX00-KL99");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "XX00-KN00");

    key.setKey("CA16-FF04-KL98");
    key.IncrementKey();
    QCOMPARE(key.getKey(), "CA16-FF04-KL99");

    Key key2(5, "***", 'C', 'V', '1', '8', "SEIHMPQ", "257", 3, 1);
    key2.setKey("VVV1***DDD6***FFF3");
    key2.IncrementKey();
    QCOMPARE(key2.getKey(), "VVV1***DDD6***FFF4");

    Key key3(5);
    key3.setKey("ZZ99-ZZ99-ZZ99-ZZ99");
    key3.IncrementKey();
    QCOMPARE(key3.getKey(), "AA01-AA00-AA00-AA00-AA00");

    Key key4(5, "");
    key4.setKey("ZZ99ZZ99ZZ99ZZ99");
    key4.IncrementKey();
    QCOMPARE(key4.getKey(), "AA01AA00AA00AA00AA00");
}

void Test_Key::DecrementKey()
{
    Key key;
    key.setKey("AA10");
    QCOMPARE(key.getKey(), "AA10");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "AA09");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "AA08");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "AA06");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "AA04");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "AA02");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "AA01");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "AA00");

    key.DecrementKey();
    key.DecrementKey();
    key.DecrementKey();
    QCOMPARE(key.getKey(), "AA00");

    key.setKey("AB00");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "AA99");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "AA98");

    key.setKey("AA02-AA00");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "AA01-ZZ99");

    key.setKey("AA01-AA00");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "ZZ99");

    key.setKey("AA01-AA00-AA00");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "ZZ99-ZZ99");

    key.setKey("ZZ99-ZZ99-ZZ99");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "ZZ99-ZZ99-ZZ98");

    key.setKey("XX00-KL00");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "XX00-KK99");

    key.setKey("CA16-FF04-KL90");
    key.DecrementKey();
    QCOMPARE(key.getKey(), "CA16-FF04-KL89");

    Key key2(5, "***", 'C', 'V', '1', '8', "SEIHMPQ", "257", 3, 1);
    key2.setKey("VVV1***DDD6***FFF1");
    key2.DecrementKey();
    QCOMPARE(key2.getKey(), "VVV1***DDD6***FFD8");

    Key key3(5);
    key3.setKey("AA01-AA00-AA00-AA00-AA00");
    key3.DecrementKey();
    QCOMPARE(key3.getKey(), "ZZ99-ZZ99-ZZ99-ZZ99");

    Key key4(5, "");
    key4.setKey("AA01AA00AA00AA00AA00");
    key4.DecrementKey();
    QCOMPARE(key4.getKey(), "ZZ99ZZ99ZZ99ZZ99");
}

QTEST_APPLESS_MAIN(Test_Key)

#include "tst_test_key.moc"
