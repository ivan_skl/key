#include "key.h"

void run(Key &k)
{
	while (1)
	{					
		std::cout <<  k.getKey() << '\n';
		k.IncrementKey();
		
		std::this_thread::sleep_for(std::chrono::milliseconds(500));
	}
}

void run2(Key &k)
{
	while (1)
	{					
		std::cout <<  k.getKey() << '\n';
		k.DecrementKey();
		
		std::this_thread::sleep_for(std::chrono::seconds(3));
	}
}

int main()
{
	Key key;
	key.setKey("AA01-AA00-AA00");
	key.DecrementKey();
	
	
	std::thread t1(run, std::ref(key));
	std::thread t2(run2, std::ref(key));
	t1.join();
	t2.join();
	
	return 0;
}
