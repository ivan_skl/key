CC=g++
CFLAGS=-c -Wall -std=c++11
LFLAGS=-pthread

all: key

key: main.o key.o
	$(CC) $(LFLAGS) main.o key.o -o key

main.o: main.cpp
	$(CC) $(CFLAGS) main.cpp

key.o: key.cpp
	$(CC) $(CFLAGS) key.cpp

clean:
	rm -rf *.o key
