#ifndef KEY_H
#define KEY_H

#include <iostream>
#include <string>
#include <vector>
#include <array>
#include <mutex>
#include <thread>
#include <chrono>
#include <cassert>

class Key 
{
public:
	explicit Key(int number_of_groups_max_ = 3, 
			const std::string& delimiter_ = "-", 
			char table1_begin_ = 'A', 
			char table1_end_ = 'Z', 
			char table2_begin_ = '0', 
			char table2_end_ = '9', 
			const std::string& excepts1_ = "EIHMP", 
			const std::string& excepts2_ = "357", 
			int chars_in_row1_ = 2, 
			int chars_in_row2_ = 2 ); 
	~Key();

	void setKey(const std::string& key);

	void setKey(const Key& key);

	std::string getKey() const;

	void IncrementKey();

	void DecrementKey();

	Key(Key& k);

	Key(Key&& k);

	Key& operator=(Key& k);

	Key& operator=(Key&& k);

private:
	mutable std::mutex m_mutex;

	int number_of_groups_max;
	std::vector<std::string> group;
	std::string delimiter;
	std::array<char, 2> table_begin;	
	std::array<char, 2> table_end;	
	std::array<std::string, 2> excepts;	
	std::array<int, 2> chars_in_row;
	int n_group;
	int n_char;
	std::string first_key;
	std::string first_key_plus_one;
	std::string last_key;

	void Init();

	void InitCopy(Key &k);

	void InitMove(Key &k);

	bool CheckTable(int n, char c) const;
};	

#endif // KEY_H

