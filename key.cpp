#include "key.h"

Key::Key(int number_of_groups_max_, 
		const std::string& delimiter_, 
		char table1_begin_, 
		char table1_end_ , 
		char table2_begin_, 
		char table2_end_, 
		const std::string& excepts1_, 
		const std::string& excepts2_, 
		int chars_in_row1_, 
		int chars_in_row2_) : 
	number_of_groups_max(number_of_groups_max_),
	delimiter(delimiter_)                      
{
	table_begin.at(0) = table1_begin_; 
	table_begin.at(1) = table2_begin_;

	table_end.at(0) = table1_end_;
	table_end.at(1) = table2_end_;

	excepts.at(0) = excepts1_;
	excepts.at(1) = excepts2_;

	chars_in_row.at(0) = chars_in_row1_;
	chars_in_row.at(1) = chars_in_row2_;

	assert(number_of_groups_max_ >= 0 && number_of_groups_max_ <= 256);

	for (int n = 0; n < 2; n++)
	{
		for (unsigned int i = 0; i < excepts.at(n).length(); i++)
		{
			assert(excepts.at(n).at(i) != table_begin.at(n) && excepts.at(n).at(i) != table_end.at(n));
		}
	}

	Init();
}

Key::~Key()
{

}

void Key::setKey(const std::string& key)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	group.clear();
	group.resize(number_of_groups_max);
	int key_len = key.length();
	int delimiter_len = delimiter.length();
	int group_len = chars_in_row.at(0) + chars_in_row.at(1);
	int num_group = 0;
	int i = key_len-1;
	int key_len_max = group_len * number_of_groups_max + delimiter_len * (number_of_groups_max - 1);
	
	assert(key_len >= group_len && key_len <= key_len_max);
	
	while (i >= 0)
	{

		for (int r1 = i - (group_len - 1); r1 <= i - chars_in_row.at(1); r1++)
		{
			char c1 = key.at(r1);

			if (CheckTable(0, c1))
			{
				group.at(num_group) += c1;
			}
			else
			{
				assert(0 && "Wrong key.");
			}
		}
        

		for (int r2 = i - chars_in_row.at(1) + 1; r2 <= i; r2++)
		{
			char c2 = key.at(r2);

			if (CheckTable(1, c2))
			{
				group.at(num_group) += c2;
			}
			else
			{
				assert(0 && "Wrong key.");
			}
		}
        
		i = i - group_len;
		  
		if (delimiter_len == 0)
		{
			num_group += 1;
			continue;
		}
		
		assert(n_group < number_of_groups_max);

		if (i < 0) break;

		for (int d = delimiter_len - 1; d >= 0 ; d--)
		{
			assert(key.at(i) == delimiter.at(d));		    
		    	--i;
		}
        
		num_group += 1;
		assert(n_group < number_of_groups_max && i >= group_len - 1);
	}
}

void Key::setKey(const Key& key)
{
	std::lock_guard<std::mutex> lock(m_mutex);
    	setKey(key.getKey());
}

std::string Key::getKey() const
{
	std::lock_guard<std::mutex> lock(m_mutex);
	std::string key;

	for (int i = group.size() - 1; i >= 0; i--)
	{		
	   if (!group.at(i).empty())
	   {
		  key += group.at(i);

		  if (i != 0) 
		  {
			key += delimiter;
		  }

	   }        
	}

	return key;
}

void Key::IncrementKey()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	bool full_last_key = true;
	for (int n = group.size() - 1; n >= 0; n--)
	{
		if (group.at(n) != last_key)
		{
			full_last_key = false;
			break;
		}
	}

	if (full_last_key)
	{
		return;
	}

	while (group.at(n_group) == last_key) 
	{
		group.at(n_group) = first_key;
		n_group++;
		
		if (group.at(n_group).empty()) 
		{
			group.at(n_group) = first_key;
			break;
		}
	}

	for (int i=0; i < chars_in_row.at(0) + chars_in_row.at(1); i++)
	{
		if (n_char < chars_in_row.at(0))
		{
			if (group.at(n_group).at(n_char) == table_end.at(0)) 
			{ 
				group.at(n_group).at(n_char) = table_begin.at(0);
				--n_char;
			}
		}
		else
		{
			if (group.at(n_group).at(n_char) == table_end.at(1)) 
			{ 
				group.at(n_group).at(n_char) = table_begin.at(1);
				--n_char;
			}
		}
	}

	group.at(n_group).at(n_char)++; 

	if (n_char < chars_in_row.at(0))
	{
		if (!CheckTable(0, group.at(n_group).at(n_char))) 
		{
			if (group.at(n_group).at(n_char) != table_end.at(0)) 
			{
				group.at(n_group).at(n_char)++;
			}
		}
	}
	else
	{
		if (!CheckTable(1, group.at(n_group).at(n_char))) 
		{
			if (group.at(n_group).at(n_char) != table_end.at(1)) 
			{
				group.at(n_group).at(n_char)++;
			}
		}
	}	

	n_char = chars_in_row.at(0) + chars_in_row.at(1) - 1;
	n_group = 0;
}


void Key::DecrementKey()
{
	std::lock_guard<std::mutex> lock(m_mutex);
	bool full_last_key = true;
	for (int n = group.size() - 1; n > 0; n--)
	{
		if (group.at(n) != "")
		{
			full_last_key = false;
			break;
		}
	}

	if (group.at(0) != first_key)
	{
		full_last_key = false;
	}

	if (full_last_key)
	{
		return;
	}

	while (group.at(n_group) == first_key) 
	{
		group.at(n_group) = last_key;
			n_group++;

		if (group.at(n_group) == first_key_plus_one) 
		{
			group.at(n_group) = "";
			n_group = 0;
			return;
		}
	}
	

	for (int i = 0; i < chars_in_row.at(0) + chars_in_row.at(1); i++)
	{
		if (n_char < chars_in_row.at(0))
		{
			if (group.at(n_group).at(n_char) == table_begin.at(0)) 
			{ 
				group.at(n_group).at(n_char) = table_end.at(0);
				--n_char;
			}
		}
		else
		{
			if (group.at(n_group).at(n_char) == table_begin.at(1)) 
			{ 
				group.at(n_group).at(n_char) = table_end.at(1);
				--n_char;
			}
		}
	}

	group.at(n_group).at(n_char)--; 
	
	if (n_char < chars_in_row.at(0))
	{
		if (!CheckTable(0, group.at(n_group).at(n_char))) 
		{
			if (group.at(n_group).at(n_char) != table_end.at(0)) 
			{
				group.at(n_group).at(n_char)--;
			}
		}
	}
	else
	{
		if (!CheckTable(1, group.at(n_group).at(n_char))) 
		{
			if (group.at(n_group).at(n_char) != table_end.at(1)) 
			{
				group.at(n_group).at(n_char)--;
			}
		}
	}

	n_char = chars_in_row.at(0) + chars_in_row.at(1) - 1;
	n_group = 0;
}

Key::Key(Key &k) 
{
	std::lock_guard<std::mutex> lock(m_mutex);
	InitCopy(k);
}

Key::Key(Key&& k) 
{
	std::lock_guard<std::mutex> lock(m_mutex);
	InitMove(k);
}

Key& Key::operator=(Key& k) 
{
	std::lock_guard<std::mutex> lock(m_mutex);
	InitCopy(k);

	return *this;
}

Key& Key::operator=(Key&& k)
{
	std::lock_guard<std::mutex> lock(m_mutex);
	if (&k == this)
	{
		return *this;
	}

	InitMove(k);

	return *this;
}

void Key::Init()
{
	group.resize(number_of_groups_max);
	n_group = 0;
	n_char = chars_in_row.at(0) + chars_in_row.at(1) - 1;

	for (int n = 0; n < 2; n++)
	{
		for (int i = 0; i < chars_in_row.at(n); i++)
		{
			first_key += table_begin.at(n);
			last_key += table_end.at(n);
		}
	}

	first_key_plus_one = first_key;
	first_key_plus_one.at(chars_in_row.at(0) + chars_in_row.at(1) - 1) = table_begin.at(1) + 1;
	group.at(0) = first_key;
}

void Key::InitCopy(Key &k)
{
	number_of_groups_max = k.number_of_groups_max;
	delimiter = k.delimiter;
	table_begin.at(0) = k.table_begin.at(0);
	table_end.at(0) = k.table_end.at(0);
	table_begin.at(1) = k.table_begin.at(1);
	table_end.at(1) = k.table_end.at(1);
	excepts.at(0) = k.excepts.at(0);
	excepts.at(1) = k.excepts.at(1);
	chars_in_row.at(0) = k.chars_in_row.at(0);
	chars_in_row.at(1) = k.chars_in_row.at(1);

	group = k.group;
	n_group = k.n_group;
	n_char = k.n_char;
	first_key = k.first_key;
	first_key_plus_one = k.first_key_plus_one;
	last_key = k.last_key;		
}

void Key::InitMove(Key &k)
{
	InitCopy(k);
	k.number_of_groups_max = 3;
	k.delimiter = "-";
	k.table_begin.at(0) = 'A';
	k.table_end.at(0) = 'Z';
	k.table_begin.at(1) = '0';
	k.table_end.at(1) = '9';
	k.excepts.at(0) = "EIHMP";
	k.excepts.at(1) = "357";
	k.chars_in_row.at(0) = 2;
	k.chars_in_row.at(1) = 2;

	k.group.clear();
	k.group.resize(k.number_of_groups_max);
	k.n_group = 0;
	k.n_char = k.chars_in_row.at(0) + k.chars_in_row.at(1) - 1;
	k.first_key = "";
	k.last_key = "";

	for (int n = 0; n < 2; n++)
	{
		for (int i = 0; i < k.chars_in_row.at(n); i++)
		{
			k.first_key += k.table_begin.at(n);
			k.last_key += k.table_end.at(n);
		}
	}

	k.first_key_plus_one = k.first_key;
	k.first_key_plus_one.at(k.chars_in_row.at(0) + k.chars_in_row.at(1) - 1) = k.table_begin.at(1) + 1;
	k.group.at(0) = k.first_key;		
}

bool Key::CheckTable(int n, char c) const
{
	for (unsigned int i = 0; i < excepts.at(n).length(); i++)
	{
		if (excepts.at(n).at(i) == c)
		{
			return false;
		}
	}

	if (c >= table_begin.at(n) && c <= table_end.at(n)) 
	{
		return true;
	} 
	else 
	{
		return false;
	}
}



